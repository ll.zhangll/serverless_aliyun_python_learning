import requests
import json
from . import serialization_tool

fea_extract_url = 'http://123.57.220.156:5005/fea_extract'
feas_extract_url = 'http://123.57.220.156:5005/feas_extract'

def getFeaFromSamllBase64Img(base64_small_img):
    '''提取一张小图的特征值'''
    req_data = json.dumps({"image": base64_small_img})
    r = requests.post(fea_extract_url, data=req_data)
    res_json = json.loads(r.text)
    # one_fea = np.frombuffer(base64.b64decode(res_json['feat']), np.float32)
    one_fea = serialization_tool.strSerializTodict(res_json['feat'])
    return one_fea

def getFeaFromlargeBaseImg(base64_large_img, bboxes):
    '''
    提取一张大图的特征值
    base64_large_img: base64encode
    bboxes = [[743, 234, 1213, 740], [322, 193, 639, 505]]
    '''
    req_data = json.dumps({'image': base64_large_img, 'bboxes': bboxes})
    r = requests.post(feas_extract_url, data=req_data)
    res_json = json.loads(r.text)
    feas = serialization_tool.strSerializTodict(res_json['feats'])
    return feas