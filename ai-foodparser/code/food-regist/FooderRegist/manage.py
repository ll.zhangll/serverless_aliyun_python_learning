# -*- coding: utf-8 -*-
import base64
from hashlib import md5
from FooderUtil.feaextract import getFeaFromSamllBase64Img
from FooderUtil.serialization_tool import dictSerializToStr
from FooderUtil.uuid_tool import getuuId

class FooderRegistError(RuntimeError):
    def __init__(self, errMsg):
        self.errMsg = errMsg

    def data(self, errDetails):
        self.errDetails = errDetails
        return self

ErrRegistReqParam = FooderRegistError("缺少必选的请求参数")

class FoodRegist(object):
    def __init__(self):
        pass

    def regist(self, j_data, context, shopId, foodRedis, foodTable, foodOss):
        '''
        菜品信息注册
        一、无图的菜品注册
            1. 判断表里该菜名是否存在，不存在则注册，存在则返回 exist
        二、有图的菜品注册
            1. http到GPU提取特征
            2. 根据菜名判断是否已存在，不存在则插入到菜品识别表，存在则返回dishId
            3. 计算feaId=md5(base64_small_img)，将特征图片上传到oss，获取feaImgUrl
            4. 将feaId, dishId, feaImgUrl 插入到菜品特征表
            5. 将dishId, fea插入到Redis
        三、判断在注册时是否传入菜品类别
        四、判断在注册时是否传入菜单
        '''
        if 'dishName' not in j_data.keys() or \
           'dishPrice' not in j_data.keys() or \
           'goodsType' not in j_data.keys():
            raise ErrRegistReqParam.data(f'缺少dishName or dishPrice or goodsType')
        dishName = j_data['dishName']
        dishPrice = int(j_data['dishPrice'])
        goodsType = int(j_data['goodsType'])
        base64_small_img = ''
        if 'image' in j_data.keys():
            base64_small_img = j_data['image']

        dishId = getuuId()
        primary_key = [('shopId', shopId), ('dishId', dishId)]
        attribute_columns = [('dishName', dishName), ('dishPrice', dishPrice), \
                                ('dishTmpPrice', dishPrice), ('goodsType', goodsType)]
        if len(base64_small_img) < 100:
            foodTable.put_row_dishInfo_noImg(context, shopId, dishName, primary_key, attribute_columns)
        else:
            # 1. http到GPU提取特征
            oneFea = getFeaFromSamllBase64Img(base64_small_img)
            # 2. 根据菜名判断是否已存在，不存在则插入到菜品识别表，存在则返回dishId
            exist_dishId = foodTable.put_row_dishInfo_Img(context, shopId, dishName, primary_key, attribute_columns)
            if exist_dishId is not None:
                dishId = exist_dishId
            # 3. 计算feaId=md5(base64_small_img)，将特征图片上传到oss，获取feaImgUrl
            feaId = md5(base64_small_img.encode('utf-8')).hexdigest().lower()
            oss_file_name = f'{shopId}/{feaId}.jpg'
            feaImgUrl = foodOss.put_rawImg_object(context, oss_file_name, base64.b64decode(base64_small_img))
            # 4. 将feaId, dishId, feaImgUrl 插入到菜品特征表
            primary_key = [('shopId', shopId), ('feaId', feaId), ('dishId', dishId)]
            attribute_columns = [('fea', dictSerializToStr(oneFea)), ('feaImgUrl', feaImgUrl)]
            foodTable.put_row_feaInfo(context, primary_key, attribute_columns)
            # 5. 将dishId, fea插入到Redis
            foodRedis.fea_regist(shopId, f'{dishId}:{feaId}', oneFea)
        if 'classId' in j_data.keys():
            classId = j_data['classId']
            if len(classId) > 0:
                pass

        if 'menuId' in j_data.keys():
            menuId = j_data['menuId']
            if len(menuId) > 0:
                pass

        return dishId
