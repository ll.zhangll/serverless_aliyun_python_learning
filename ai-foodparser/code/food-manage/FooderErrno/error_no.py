# -*- coding: utf-8 -*-
from .code import NewError 

OK = NewError(0, "成功")

ErrServerRuntime = NewError(10001, "服务异常")

ErrServerSignIsNone = NewError(10002, "签名不能为空!")
ErrServerSignError = NewError(10003, "签名错误!")



