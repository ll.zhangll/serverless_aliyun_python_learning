#!/usr/bin/env python
# -*- coding: utf-8 -*-
class NewError(object):
    def __init__(self, code, message):
        self.result = {'code': code, 'message': message, 'data': {}}

    def data(self, data={}):
        if data is None:
            data = {}
        self.result['data'] = data
        return self.result