#!/usr/bin/env python
# -*- coding: utf-8 -*-
import base64
import json
import logging
import traceback
from FooderManage.manage import FoodManage
from FooderErrno import error_no


# if you open the initializer feature, please implement the initializer function, as below:
def initializer(context):
    logger = logging.getLogger()
    logger.info('initializing')


def handler(event, context):
    try:
        logger = logging.getLogger()
        evt_list = json.loads(event)
        # print('handler event========', event)
        action = evt_list['headers']['action']
        body = base64.b64decode(evt_list['body'])
        return getattr(FoodManage(), action)(event, context, body)

    except Exception as e:
        logger.error(traceback.format_exc())
        result = {'errorMessage': traceback.format_exc()}
        return error_no.ErrServerRuntime.data(result)
