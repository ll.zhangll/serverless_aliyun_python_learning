# -*- coding: utf-8 -*-
import logging
import traceback
import json, os, base64, cv2, uuid
import numpy as np
from FooderUtil.table_util import TableUtil
from FooderUtil.oss_util import OSSUtil
from FooderUtil.redis_util import RedisUtil
from FooderUtil.authentication_tool import filter

LOCAL = bool(os.getenv("local", ""))

def initializer(context):
    logger = logging.getLogger()
    try:
        if LOCAL:
            oss_endpoint = os.environ['OSS_ENDPOINT_LOCAL']
            table_endpoint = os.environ['TABLE_ENDPOINT_LOCAL']
        else:
            oss_endpoint = os.environ['OSS_ENDPOINT']
            table_endpoint = os.environ['TABLE_ENDPOINT']
        bucket_name = os.environ['BUCKET_NAME']
        table_name = os.environ['TABLE_INSTANCE_NAME']
        global foodOss
        foodOss = OSSUtil(oss_endpoint, bucket_name, context)
        global foodTable
        foodTable = TableUtil(table_endpoint, table_name, context)

        redis_host = os.environ['REDIS_HOST_LOCAL']
        redis_password = os.environ['REDIS_PASSWORD']
        redis_port = os.environ['REDIS_PORT']
        global foodRedis
        foodRedis = RedisUtil(redis_host, redis_password, redis_port)
    except Exception as e:
        logger.error('initializer error', traceback.format_exc())


def handler(event, context):
    # redis的使用
    if 1:
        if 0:
            foodRedis.moni()
        if 1:
            foodRedis.hash_get_all_keys()
    # oss的使用
    if 0:
        if LOCAL:
            with open('event.evt', 'r') as f:
                event = f.read()
        evt_list = json.loads(event)
        isbase64 = evt_list['isBase64Encoded']       
        if isbase64:
            body = base64.b64decode(evt_list['body'])
        else:
            body = evt_list['body']
        j_data = json.loads(body)
        base64_small_img = j_data['image']
        imgData = base64.b64decode(base64_small_img)
        # nparr = np.frombuffer(imgData, np.uint8)
        # img_np = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
        # (row, col, chan) = img_np.shape

        file_name = '商家A/md51.jpg'
        # result = foodOss.put_rawImg_object(context, file_name, imgData)
        # print('----', result)

        object_stream = foodOss.get_object(context, file_name)
        nparr = np.frombuffer(object_stream.read(), np.uint8)
        img_np = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
        # 如何验证img_np

        # foodOss.delete_object(context, file_name)
        # return 'hello world'
    # tablestore 使用
    if 0:
        at = foodTable.get_table_list(context)
        print('获取实例下所有表的表名', at)
        # 插入一条菜品数据
        dishId1 = str(uuid.uuid1())
        dishName1 = f'爆炒土豆丝{dishId1}'
        if 0:
            primary_key = [('shopId', '商家A'), ('dishId', dishId1)]
            attribute_columns = [('dishName', dishName1), ('dishPrice', 1200), ('dishTmpPrice', 1200), ('goodsType', 1)]
            foodTable.put_row_dishInfo(context, primary_key, attribute_columns)
        # 插入一条菜品特征数据
        if 0:
            primary_key = [('shopId', '商家A'), ('feaId', dishId1 + 'md5'), ('dishId', dishId1)]
            attribute_columns = [('feaImgUrl', 'oss url')]
            foodTable.put_row_feaInfo(context, primary_key, attribute_columns)
        # 查询菜品详情信息
        if 0:
            foodTable.get_dish_details(context, '商家A', 'bfb048d6-f209-11eb-b658-0242ac110002')
            # print('菜品详情=', ddd)
        # 查询全部
        if 0:
            table_name = 'dish_list'
            index_name = ''
            foodTable.get_MatchAllQuery(context, table_name)
        # 多元索引查询
        if 0:
            table_name = 'dish_fea_list'
            index_name = 'shopId_dishId_index'
            # foodTable.get_MatchAllQuery(context, table_name, index_name)
            foodTable.get_MatchAllQuery12(context, table_name, index_name)
        # 存在性判断
        if 1:
            foodTable.test_exist_dish_list_dishName()

