import redis
import pickle

class RedisUtil:
    def __init__(self, host, password, port):
        if host is None or \
            password is None or\
            port is None:
            raise RuntimeError('param has None')
        self.host = host
        self.password = password
        self.port = port
        self.refresh_init()

    # 重新初始化
    def refresh_init(self):
        self.conn_pool = redis.ConnectionPool(host=self.host,
                                              password=self.password,
                                              port=self.port,
                                              db=1,
                                              decode_responses=True)

    def hash_get_all_keys(self):
        """获取当前数据库中key的数目"""
        '''获取所有符合规则的key='''
        if self.conn_pool is None:
            self.refresh_init()

        if self.conn_pool is not None:    
            r = redis.Redis(connection_pool=self.conn_pool)
            if r is None:
                raise RuntimeError('redis.Redis is None')
            hShopId = 'ai-food-shop:商家A'
            print('获取当前数据库中key的数目=', r.dbsize())
            print('获取所有符合规则的key=', r.keys())
            print('获取name=商家A的所有key=', r.hkeys(hShopId))
            print('获取name=商家A的repo_count键的值=', r.hget(hShopId, 'repo_count'))
            # print('获取name=商家A的所有映射键值对=', r.hgetall(hShopId))
            # print('获取name=商家A的all_repo_ids键的值=', pickle.loads(r.hget(hShopId, 'all_repo_ids').encode('latin1')))
            # print('获取name=商家A的all_repo_feats键的值=', pickle.loads(r.hget('商家A', 'all_repo_feats').encode('latin1')))
            # print('获取name=商家A的repo键的值=', pickle.loads(r.hget('商家A', 'repo').encode('latin1')))
            if 0:
                r.hdel(hShopId, 'repo')
                r.hdel(hShopId, 'all_repo_ids')
                r.hdel(hShopId, 'all_repo_feats')
                r.hdel(hShopId, 'repo_count')

    def moni(self):
        r = redis.Redis(connection_pool=self.conn_pool)
        name = f'ai-food-device:deviceId001'
        r.set(name, '商家A')
    
    def hash_get_data(self, name, key):
        '''获取Redis中，name=xx, key=xx 的数据'''
        if name is None or key is None:
            return RuntimeError('param has None')

        if self.conn_pool is None:
            self.refresh_init()

        if self.conn_pool is not None:    
            r = redis.Redis(connection_pool=self.conn_pool)
            if r is None:
                raise RuntimeError('redis.Redis is None')
            if not r.hexists(name, key):
                raise RuntimeError(f'{name}-{key} not exist')
            return r.hget(name, key)
        raise RuntimeError('param has None')

    def hash_set_data(self, name, key, value):
        '''向 Redis name=xx, key=xx 中 value=xx 的数据'''
        if name is None or key is None or value is None:
            return RuntimeError('param has None')

        if self.conn_pool is None:
            self.refresh_init()

        if self.conn_pool is not None:
            r = redis.Redis(connection_pool=self.conn_pool)
            if r is None:
                raise RuntimeError('redis.Redis is None')
            return r.hset(name, key, value)
        return None
