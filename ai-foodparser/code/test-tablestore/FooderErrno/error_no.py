# -*- coding: utf-8 -*-
class NewError(object):
    def __init__(self, code, message):
        self.result = {'code': code, 'message': message, 'data': {}}

    def data(self, data={}):
        if data is None:
            data = {}
        self.result['data'] = data
        return self.result


OK = NewError(0, "成功")

ErrServerTokenNull = NewError(10001, "Access_token不能为空")
ErrServerTokenParse = NewError(10002, "Access_token解析失败")
ErrServerTokenPackage = NewError(10003, "token格式错误")
ErrServerUnauthorized = NewError(10004, "设备未授权")
ErrServerTokenBody = NewError(10005, "消息体内容被篡改")


ErrServerRuntime = NewError(10006, "服务异常")
