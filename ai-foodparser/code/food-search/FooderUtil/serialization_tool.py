# -*- coding: utf-8 -*-
import pickle

def dictSerializToStr(data):
    '''
    将字典特征库序列化成str
    data=<class 'dict'> --> <class 'bytes'> --> <class 'str'>
    '''
    feas = pickle.dumps(data).decode('latin1')
    return feas

def strSerializTodict(data):
    '''
    将str反序列化成字典特征库
    <class 'str'> --> <class 'bytes'> --> <class 'dict'>
    '''
    feas = pickle.loads(data.encode('latin1'))
    return feas