# 自定义菜品识别服务平台

## 1. 鉴权认证

### 接口说明

在调用业务接口时，请求方需要对请求进行签名，服务端通过签名来校验请求的合法性

body（请求内容）：其明⽂为每次请求的具体参数，采⽤ JSON 格式。

Access_token生成规则：AES（deviceId：md5（body））

deviceId为请求设备的唯一标识，body采用JSON格式后再计算md5

Header如下：

| 参数         | 是否必选 | 类型   | 值              |
| ------------ | -------- | ------ | --------------- |
| Access_token | 是       | string | AES加密后的令牌 |

## 2. 菜品注册/添加/追加

### 接口说明

该接口实现单张菜品图片入库，入库时需要同步提交图片的菜品名称、价格等信息

### 接口要求

| 内容     | 说明                                     |
| -------- | ---------------------------------------- |
| 传输方式 | http                                     |
| 请求地址 | http://aiplat.api.xzxyun.com/food/regist |
| 请求行   | POST /food/regist                        |
| 接口鉴权 | 签名机制，详情请参考 1.鉴权认证          |
| 字符编码 | UTF-8                                    |
| 响应格式 | 统一采用JSON格式                         |
| 图片格式 | jpg/png                                  |
| 图片大小 | base64编码后大小不超过4M                 |
| 图片要求 | 入库图为经过目标检测的单个菜品图         |

### 请求参数

| 参数      | 是否必选 | 类型   | 说明                                                         |
| --------- | -------- | ------ | ------------------------------------------------------------ |
| dishName  | 是       | string | 注册时的菜品名称，菜品名称全局唯一，对于已经存在的菜名，再次注册，则会在该菜名后追加一张图片 |
| dishPrice | 是       | int    | 注册时的菜品价格，单位是分，在对已有菜品进行追加图片时，价格不会被更新 |
| goodsType | 是       | int    | 商品类型：1表示菜品，2表示酒水                               |
| image     | 否       | string | Base64编码字符串<br />如果是一整张图片注册，则必须带有coord字段<br />如果是已经过目标矩形框裁剪的单个菜品图，则不需要带coord字段<br />如果image不存在或为""，也会注册该菜品，如果菜名已经，会提示菜品已存在 |
| //coord   | 否       | list   | 注册菜品目标矩形框坐标，如果coord不存在或为[]，会提取image整图特征<br />传参示例：coord: [806, 536, 1347, 1053] //左上, 右下目标坐标值 |
| classId   | 否       | list   | 菜品类别Id集合, 注册时为菜品打类别标签，例如主食、荤菜等<br />如果classId不存在或为[]，则注册时不为菜品打类别标签。<br />传参示例：classId: [1, 2] |
| menuId    | 否       | list   | 菜单类别Id集合，注册时为菜品打菜单标签，例如早餐、午餐等<br />如果menuId不存在或为[]，则注册时不为菜品打菜单标签。<br />传参示例：menuId: [1, 2] |

**请求代码示例**

```
import requests
import base64

request_url = "https://aip.baidubce.com/rest/2.0/image-classify/v1/realtime_search/dish/add"
# 二进制方式打开图片文件
f = open('[本地文件]', 'rb')
img = base64.b64encode(f.read())

params = {"brief":1,"image":img,"sub_lib":"[appid]"}
access_token = '[调用鉴权接口获取的token]'
request_url = request_url + "?access_token=" + access_token
headers = {'content-type': 'application/x-www-form-urlencoded'}
response = requests.post(request_url, data=params, headers=headers)
if response:
    print (response.json())
```

### 返回说明

**返回参数**

| 字段    | 是否必选 | 类型   | 说明       |
| ------- | -------- | ------ | ---------- |
| code    | 是       | int    | 详见错误码 |
| message | 是       | string | 错误描述   |
| data    | 是       | object | 详情信息   |
| +dishId | 否       | string | 菜品Id     |

**返回json示例**

```
{
	"code": 0,
	"message": "",
	"data": {
		"dishId":""
	}
}
```

### 错误码

| code | 说明                      | 错误描述信息 | 解决办法   |
| ---- | ------------------------- | ------------ | ---------- |
| 1000 | Token exception           | 鉴权认证失败 | 请联系厂家 |
| 1001 | Service running exception | 服务运行异常 | 请联系厂家 |

## 3. 菜品检索

### 接口说明

该接口可实现基于输入一帧图片及其目标检测信息，返回菜品的检索结果

### 接口要求

| 内容     | 说明                                     |
| -------- | ---------------------------------------- |
| 传输方式 | http                                     |
| 请求地址 | http://aiplat.api.xzxyun.com/food/search |
| 请求行   | POST /food/search                        |
| 接口鉴权 | 签名机制，详情请参考 1.鉴权认证          |
| 字符编码 | UTF-8                                    |
| 响应格式 | 统一采用JSON格式                         |
| 图片格式 | jpg/png                                  |
| 图片大小 | base64编码后大小不超过4M                 |
| 图片要求 | 清晰的菜品图片                           |

### 请求参数

| 参数       | 是否必选 | 类型     | 说明                                    |
| ---------- | -------- | -------- | --------------------------------------- |
| image      | 是       | string   | Base64编码字符串，必须带有plateData字段 |
| targetData | 是       | object[] | 菜品在图片中的位置                      |
| +left      | 是       | int      | 水平坐标(左上角原点)                    |
| +top       | 是       | int      | 垂直坐标(左上角原点)                    |
| +right     | 是       | int      | 水平坐标(右下角原点)                    |
| +bottom    | 是       | int      | 垂直坐标(右下角原点)                    |

**请求json示例**

```
{
	"image": "base64 image",
	"plateData": [{
			"left": 743,
			"right": 1213,
			"top": 234,
			"bottom": 740
		},
		{
			"score": 0.9236,
			"left": 322,
			"right": 639,
			"top": 193,
			"bottom": 505
		}
	]
}
```

### 返回说明

| 字段         | 是否必选 | 类型             | 说明       |
| ------------ | -------- | ---------------- | ---------- |
| code         | 是       | int              | 详见错误码 |
| message      | 是       | string           | 描述信息   |
| data         | 是       | object{}         | 详情信息   |
| +errCode     |          |                  |            |
| +errMessage  |          |                  |            |
| +resultNum   |          |                  |            |
| +result      |          | object[]         |            |
| ++location   |          | object{}         |            |
| +++left      |          | int              |            |
| +++top       |          | int              |            |
| +++right     |          | int              |            |
| +++bottom    |          | int              |            |
| ++dishes     |          | object[{},{}...] |            |
| +++dishId    |          | string           |            |
| +++socre     |          | int              |            |
| +++dishName  |          | string           |            |
| +++dishPrice |          | int              |            |
| +++imageUrl  |          | string           |            |

**返回json示例**

```
{
	"code": 0,
	"message": "success",
	"data": {
		"errCode": 0/1/2/3/4,
		"errMessage": "成功/图像中未检测到菜品/图像中检测到菜品，但是空菜单/图像中未检测到菜品，有菜单/什么都未检测到",
		"resultNum": 1,
		"result": [{
			"location": {
				"left": 12,
				"top": 30,
				"right": 103,
				"bottom": 100
			},
			"dishes": [{
				"dishId": 1,
				"score": 98.88,
				"dishName": "包子",
				"dishPrice": 1300,	
				"imageUrl": "https://xxxxx.jpg"
			}]
		}]
	}
}
```

### 错误码

| code | 说明 | 错误描述信息 | 解决办法 |
| ---- | ---- | ------------ | -------- |
|      |      |              |          |



