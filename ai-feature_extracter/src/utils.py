# -*- coding: utf-8 -*-
import pickle


def get_cv_image(img_data, img_rows, img_cols, format):
    assert format in [0, 1]
    if format == 0:
        return get_cv_image_from_rgb24(img_data, img_rows, img_cols)
    elif format == 1:
        return get_cv_image_from_rgb32(img_data, img_rows, img_cols)


# 将字典特征库序列化成str
def feaSerializToStr(data):
    # data=<class 'dict'> --> <class 'bytes'> --> <class 'str'>
    feas = pickle.dumps(data).decode('latin1')
    return feas


# 将str反序列化成字典特征库
def strSerializToFea(data):
    # <class 'str'> --> <class 'bytes'> --> <class 'dict'>
    feas = pickle.loads(data.encode('latin1'))
    return feas


def get_cv_image_from_rgb32(img_data, img_rows, img_cols):
    img = img_data.reshape([img_rows, img_cols, 4])
    img = img[:, :, :3]
    return img


def get_cv_image_from_rgb24(img_data, img_rows, img_cols):
    # RGB24 is formatted in RGBRGB...
    img = img_data.reshape([img_rows, img_cols, 3])
    # img = img[:, :, [2, 1, 0]]
    return img
