import numpy as np
import base64
import pickle

fea_base641 = 'XbPrPlAwiT8D7JDA7MSzwCQoOD8mYxhAENOBQIicMkCpLKXATkHjv4sopMCg4V++j1SpwBN/2j6PqBfAaRA2P8oiDkALuABAPzZeQMDp977DKpPAyyjLQB1g4D8Yz/E/kwBkwI2F2D4lJmDAMolRQPNt2b5/paxAUoBrwJhl2L5AibC/g6uhP9XEVL9Ct6RAbkVTP+qhkz4Xyoy+iscsQIW4tr8zloDA/cHRv6LIUz89V79AKP2NwBRvN0DH8Lm/fwcfQE9m+L+jRO0/aaeBv1pJ7z8xmsg/RHJZv8pPp8ByqETAQER7Oo2Az78nj+u/GA+Ov6uwLkAIiTPAJAHHvmRIEcC5DYLAbUi5PoNUgj9vkoNAmvCZv1Q7Vj9xvBg/mJRZQC88Lz/sW8y/gn6PQDgZRcCGOve/We2VvyHquUCajk7APlsGQBkmDkBsiD4/Dd8YPXr/yr6m+xm+QEz/wIHnHUBmsOw/FcHHP4Ow7j/6xIe+S68FQMSzYD9jLChAr3mdPykdEUAH7wVAS31VPlZ8NUBA4ybAyT4+wD94U8CiseM/aQHcP8/DPEDSr8k/pPnIP82dDkDFCZI+TOhtv7Mwnr8ARCfA8tsuQAoqqD36DqG+ATWuwApiWEDsxiXAh2CJwHnT4UAA1Jw/79xZwISvOsAY32++sLuOP2hLoT8qik9AXIQbP10VHsDVWZdAXicCwEYtY8DzokBALsPmvii+3b+K3qK+IpSUv8zHgb/m176/ckwLwFSEaECBU5O/eWMPv1JgmMA0j4fA00zrv1vfmb9l1JNADI+rQDPIMUD3Ods/BizVP8VOGEBlGE7Aqfbov1c2sz7h+BBA2inRv86ZRTtOIRVAdjKUvxos+j9a5CVACyFSwDrPEUB1KIc/AGcEwJh8Sj9sL4vAlZlewHb5Ub+xRvc+iPUxvu6Uyj9Y/YtArYVQQEL3er9zM0I+LZmrPxlcc0CMe/282DxQPjwneb6gtMHAUx+NPzsw674YILHAx5/EQO3CL8Cg5jTAKSLIv4q4FMBSPZu+0Cjhv7P4MD/gss0+DncVQOzTsj/LIHRAPCtswBdNB8B3uaI/ogssQI6Kdr+Rfna+kGdHv2r//L/GoNi/qU2PwBHNCsDwQYk/fW+Lv9yaskAHBLs/LtuTPxR+479wfpo/P+CAwHY9tz2rWDvAmAeTPjnzjcCpqENAxqQ+P+/Eo8DXmLI/NRGsP8znpb8vF5O/GX9CPwjgbbtcpn2+72r5vxYc4799/Mo/paNHQNjLgz6049q/UJ/AvzYkqL+WhwU+tSWXPyUEH0Bj8xrANDSYQMqkcr9L+HZAlL4aP3TVsL5wHDJAjlfsPx9tw71ds0y/W9G8vywXwj/ny5BAosJLwGdOmT9JFdU/ygQrQBAxDcCAuo3ATj6tv7LKsz+ASku+9LSmP8a3KED02Iq+pl/ov0ReeECSm9Y/8wE7P599er9KSmPAWO3rv8U0zT/VRB5AIPmBQAliQUCxhopAKzV2vyklFb+1jug//v4/v7eyA8DBY3TADkueP2NTAUCRDpc/x2UBwLBjA7xyEge/VgTJPQr0+D4N08Y/XFRyPqQYjUCY3wVAKw2RwAf3c0ABjhhAcrm+vcHsCMC+7Fq/1gkPQI3ex7/Jy4bAOI6Yv2w+Cb2A5yDAZHlaQCuUcsAGKac/HtydPKv/Or9hV7O/bfS+vXRmW8Bt3em+7efmv3WkFEASaYRAxviYP3oxA0A2gjBAqlmnPh5t9b+0GZA/I3ftPzwGSsDiExzAfSBLv4AtPb6JAWvA1lNCPi98B8DJbKG/74Umv7BH+j/A7uw/6KQWwPwzXECRJ74+uNZIv2eSeb/HQQ7ASsejwB0qh78aPtw+JfXXv218bcA2RRo+z4OOP+HJSL+rmCvA4YAjv1T8IT8tnajAGq/qPy+uE8C2l0ZAvaiEv+1JdT+JSxjAy13lP7aKDsC3jxzACJszP4dlrz/0oOK+hZ2OP58Ghz87lky/a/rfP1hGhj8kS7g+QIfePhlnqT+jDpE+yEMVQA6vXUDa40hApU5TQBb2dkDxE7hAf9AAwGXMiD9r3uM/kjkrvf/wnL+gPZA+szx5QHznI8DO9qk+xJmXP1PXkkALTk5AK3ZqQJO6FkAbDim/CO49QKAIKcB+IFlAC5QQPwZ+nEAqAw/AtJaWQN0+yr8yIeQ+IIPvv7fbEkDeV0q/S45GwGdpTUAWxuo+mxSPvsO7UEDmBnS/PQ/pv8wBQMDARRvAyHCSwEnIH8AaqDXAgcVePzfng0Da8jY/FWEQwE4oyj4gsRW/jXy9P1WXEr+JDyRA+ICQwDO9hr9DMINALedVP4GDgT9+WQdAuLC6wP+FqL/FayhAa4wOP6I+HMDmoAfAfLz4PjCPaD+RXRhA7MtawAaCgL8HTDPAQnfjv4pzpUBwS62/8EZCQNfAcUDC8MbAOcjxvYSGNj8qzde/Ls5Cvi1MgT8Aqhg+5eeGwO/DW0BV6KQ/d+X7vzxcskDQj9m/+NeYPBqohz65vxXA4FsOQFH+yb/8gOq/FPFfwIXryT+qYaC/9Z+SP02MYECDbl0/P3odPmEpvb/81y1AoJR4QMGwm0CPmWS/d6f4v/IJ5b5zXCW//BA8P/Pil0BE5aY/cqPDPyKRIUB7SxnAE8sSP0xX1T8hwPO/p6CNwMTwWkCTbrK/lpKnv75PT0B7CiBAnDWZPwWez0DQN84/4ZeSPTWBAMA='
fea_base642 = 'n/lyP91Jkr/fkZ6//D4uwMVJTj+wto+/4jwIQObdPL7Z17vA7YGmv8KJA72S0KhAoOQmv5ncCUDCoOe+gCgMvk1dqr7qw7FAwvPlvFPCSb8gOGjAGPmtP2Kh6b/jZilAGT8+wEPYmD8Wi1rAv38Uv3QHaj4I49E/KRJtP9mfFkB3ELI/xMblv3DTn78W2fs/so8IQALzlb8kBWa/FO7gPzhh+b456kc/WySBwD6tZ0CPP28/vJEJP1uzTUD/nghASb4yP1iOir/dF54/fr8pP5bqtD0j+TlAAlK8v1rAj79kqEPAGybVPmoXpcCghoTAGaoZP5DzOUCtTzm/8w2dv5tcFsCccYHAmKUDwJIwxb7viA1AYXXEP3kCkT40lSJAm7hovhMa0b/yn6s/ULsFvziAWb40w6W/ed0gvubUbT8FepnAKnFtQHTrAEDWkwk/ygBVPgDdvb9K+bg/zh6iv7/zx7+c880/44D4v6Oh3D8K5d4//pk/QHv+1b1azbA/5VhVQJGNAcCIXs0/ZAMmQLkMvz9O79e+V4+Jv25LKsBhTz9A8cOtP4ASDEAMAig9nX3NP2+bsz+ZIJo/dngCvKYlbcAquLA+/udLP86dXj+ZLNE/FH7zv7Aj4L6LrlI/XmyewF7T5T+lDsC+FEeSvzWe+DvLnilAOvd0Pi1hAUAo1lo/M62mv0gLab+uaypAxjMmP7oO/r/oNXlA4GL5PzNE8T3wU/y/G5n2vp9ehr9y6O+/SJWuv/NC3EAWCATAX+EkQAQIwb8pMCc/5ymov9aZQL+yCT3AggfGv2cG6r7suLI9LVYEQEvT5D/x8FjApkQpQJY8Cj+1xYo/8O5aPUkmdT+ndXm/7G/Tv2JFEcALqbY/EuWfP0YiWkDqqgNAS3iJPoIIXT8g57u/iWxpwK13zz8ltvM/EQX9v/YkWkC/zaO/MFtZP7xsBEB/5Io/4+wSwFSsQb5i2xrAd9qBP2rXPEAHC8m+u6amvjNGiT952X3AcGpUPzuP4z/Y/g8/Oy5OwEmlpL3dZVU/XIrnvZfNxT7m1hk/oJUxQJiv/L8r+nlADLMEPLh8JD6J/X8/ISplPv9UsT990Jy/HpMMwIlzGEDv2aO/L3xoPcfMpr5PW1W/FEeUPtUJfT9mFjW+IfLKP14xEMCXuve+ROm7vXH/ED8Jutm/0iLDPj7wjbzjSh5AKDzQPoeEecAGVV2+eZmSvzKyUT8q/IbA6I2QvuwdpD8MDwm+rBGaP55PDz5G0jE/S28+QFOxTb+3qn691ogJwM1K4r+TwkW/ClB0QIjfB0DPfn2/JDb4P2z4zT/aXIo/JfgYv0H2E7zzHYQ/ubaAPt6joMARXIS+py2QvvU7S0CahwxAngMTwNmujkDzhcM/i5w+QP5BYjyB8knAse7uvx+r+L4VNhTA8PZxv+IBdT6Z+EpAYk5vwPf41D94BwO//CQxwEBnjT9daE3AVggvwAH+nT68jem/nTzPPZKMOr/6Rh1AObSSv5Nfmr8EvB1A8AFBv5Yx8z8l3xnAMLbhvyQHFkBPAG8+JX9Wv8hUIcDplBrAp8yLvs1oYr8mloq/adC/P4bNkL30UV6/6p6RPxP+VUBBlJU/G1ecvr1I6j3NUbK/Z2qXv7UqhcDZO96/rCiOvwTWFEAEjIG+9NFLv/W8zb/l3KU+InOpvmKApMAHq4Y+Hc8Pv4O6WcAtaQbA2N+vPP73vTyEfjpA7ERZv1tKMr7nr62/HXKUP4gyhMBB+htAoefKvZLhVMCDdw6/TeAIQIQa0j6hcKrAR4qGv5j+eD5/074/QWSsv5onq74Nwa4/yfzlv/Z2zD+P+BK/6Tg0v9Cter/MBkC/BcQOwFUSPb9g9DY8zOP9v6Xi/b9Kd78/5aJDwJ8+iz8/fS6/ilxUPhyDGEBVosO/VpFpQMcQBMCmD70/Ty/cP/7PEr9GOnrADYmYP0AYT8DG5Bu/sj2Xvow1QsDZgXrAQQATQIXB2b9LdsY/8/KXP4CUCT8rOAdA1q8SP2qUoz+Q1ZQ9nMMCP7PbUUBCl64/bYkFQKkxvD8zMh8/1iBNPoqvPz9OWb49QqqpPkOBCb8lKN++ZJC/PQ0Zir/7pv6+8Lu2P1qPhkB15vg/ExtGwPqFY0CVRTk/EQJmPu+1DMDLmhtAdfwDP4qorD+nzTVA4RprP6TMqL+beHS/Eh3rP7eWN0AOfTU/BkIPwFLP7j/iADs+aJYwQL/uWD87LTC+col0P768HMBIV/a/UmMMPxZNbj7DIda+Wb0nQHcl9z4fteY/Q9FBwF6tqD/60Oe/nsURwDNgq7+KzxY+XRK0v7L9iT/63ok/7vDNP27vnT6RqzlANqCIwHK4GcCbK0s/5SOaPpgQaD/V1jZAq3b1vtCOJDsjtKs/6AnQPSzX9r+NH2q/uoJevxKT6jwNDEu+5vNvPyqZuz8ATTg/Y1iNv08gOMDxqFK9kPUEPzwN777VroE/zst0wNQtrz87DsI/VrIpwHqAC0CHCYm/Hx5ev+iMlzxYMxW/miOzP656O71ZYqe/+t5YwKOSOr8wuOq+wx2yvznxCMCG1wtA/2NSPrY9Ez4OqznA/fYCwCdCwz8oK3S/EmcIwKui+r6uH0zAsKGlP1o5q75b8HNAHI2APTSm8z85XvC9KI28PtL48r6Hmc2/rT9iwI9DJj9C9Za/tSebvmHOYUCJHCo/H1xkv+iLMUCNdpG+J39GP9h8M78='
fea_base643 = 'ifj7v7oWpb8Zlby/dv0pwMFCiz28s9O/zOvCP4tLeT/XHfHA1j7pvvhvG7+Hhg5Aawaxvyb2A7+SsNS+5Pk4wG7/z78SZqFAij1rP2aRlr9uuELANBYCQEUrLcCmFZRAya0wwN8fY0B5R7PAdhg8wHZcvz8SLpM/Do+SvaCZ2D+DE8O+60ITwN5lRMDYwoQ+ubxtQIY/BL4LdIXA1Zk0QKmcQz+qcv2/ShxuwJFmoD/hITxAZcTpPrKXGkC+VE5AZ3bZv05agMBaXw9A2X8Zvx4Cvz/Tw3s/OYxPv3/KLz+Rnt+/Hw3MPrA8PcBhWVfAOSlAPyckWUBBF/G/EPG/v7kXTcAu/wLAz8s+vvi4mb5eclxAlsJ/vQsWS0C9PJQ/Ug9vv3yyJ77+mVi/WGz+v5NJ47/hUdC+W9h1vxFVUj8aA6nA8ch1QCeziD5hmWo+60wGwKc6YTxLVbo+UEULv5kkAb+hwSW+LBKQPswSEb9H5jW/29ZPQI8h0L/UEJ49h7BwQO3CU8DUd9c+r8haQGQXVkBs5Ju/IG8NwBzFFcC68y1A60Xrv7BVAUCal2xABNGePvr4yT+AvRA/aDKOvmgU+7+Mo0E+NpTqP7RzEz+IFVA+5usOwK5MQT8pCIO/kQzHwHPqGT8P1X8+bwSbv9T0cL+vdA1AGMAhwC+GO0Dr988+aQinPzVvPb+WBy4/6E99vwuViL9sfShAN7ZnP1+ulTzH1oS+WskZvxRN0L868SS/iLySPxRc9UBJ0JjAIGWtP/HYS8AIiCI/0UUlwNSkK8C0XMM9x+jWvtf0v7++2vA/uFUdQD1vRz9cqYzAJaGcvoioCMDxJkxA5sr9v7nm8T9hORXAxuMhv7syqb7AUw1AOZlhPxTRoz/am3lA7oHsvWI7mj9nq9C/l0mrwHHMA0CluYtArbUCwA9xLUDbHg3AomKHvpk3yz9/IXS/A+wJv3rqOb+is5fAeNDnPgs7ED+XU0vAbrSfPY8w2T7RyorADsWCQA55AMAmVI4/XVFewN48QkA2yxM+bzYLwLzEmz+2ues+4JtQQJdkjL4rPnlAr8FEPe9tsr567ia+V25mPyJlpj8jTki/d3DPv6XIHkBB6/O/NMGOvn/rG8CB4DJAyr5qvcakN75JjC4/2XkLP+C8Wb+BuDPAmVfuvpSPX74kYVfA5NwUPzK+fMDYweg+yVm5vkmAocAazXY/HWuqv6XVjT+tWSTA/IWqvi/7B0CQbAFAsNA0P+C0/75G6e0/CRwsQGrNFb4OIRVATMNmwKNbx7+44pK+wZQKQAAurj9wv6i/lWU+QC3stD6Fi72/CMYSP9QzED7nOvY/MyKbP6tME8BpaXm/wx0PP6SbFUAjlr9A2p2MwBwkXkD6p+k851BAQILMzD4fRR3A8UgZwOuuFz+adQzAfhNjP00ZAcB+1mFAoEpqv8gc3j+6Jbg/5LeCvSMnOED7clrAQwyJwICeREBn9V/AQ4kcQPAiSb9plNM/1mEBwGq4Y75cYRw/JGsLQA8PmT/ItDrA/gxivfxpmEANsxq/0yE6wPMEoMBuJIrAmhrdvVFXAr/V8iXAds2jPqtc876d88i/LBh6PTwmikCqpcg//qBHPjtYHT+NCXM/mEo8wNXPlcAP5CbAWfq3vrZxhUDghse/4u0cP0V8oz6Urlo/eh1CPwdZkMB47x6+d/8iwNrFg8CXRY2/YzReP+DZbr93DKE+slSuPhmUCr+YqiXA0GqEP+azrMDT4mJA8nuVP7SLtsD6/DLA1+6APsSbMz92NNrAVUeHvmUtpj+yLP2+5G/sPpaKB784BgRAv1oMwNn+Cb7YdPe/rYbKv+n66T+rYZY+CzLtv0H5bj8oLzzAlaVHwBYzgsBqxo6+SNTPv4X2xz/Lpec/Y59GPz198D/2jiLAjvq/P28tgL8Zkza/UmgOv60TL8BX/XTAtpyUQBv2aMCXvjW/8LqOvo5bEMAo0jPAOEUYQNmPDcB/5Zq/Vd2+PxZMjr8llB8/SPMVQPgcgD8isjC+u1YYwIgxUUDjwYg/CtEeQCDQK0AwQLG+rowoPpxmdz+8oxY9Lf4TQKV/JL+1zXU/v/yVPwuOWr/7ugrAtE98QEIKL0AFCnk/5X6CwMB5H0Br2RfAaKpWP0r/DD67Ol1A0EtOwEiMjz+DxRU/0Agxv4sEjb9Xlom+oEcaQDFzV0DxdOk/hYFIwPsI6L7EQbC/vkcNQChSA0BgaX67ieXkP7GBx78JAl3ArYwfwOuZYcBK+5K+jmw5vz6Q2b/lJsy/1kZHwOUnIMBJoi/AxkFnwIUAzr6FXS4/RnqYv/uAAj+NKuE/cojcvwdL7r9vhSRAQgiEwEkG4L9QSUM/uxjXv5paf79StaE/+Lj3PlQnQj/uAy+/vToTPnlHBsAFgFe/ARLUvd2/lL7unPA+8Mqbvi+pj74AUlk/6VLVvvC047+38ZQ/A0atP6rIjr7XNxtAGicVv+QUNb/iO0Q/kddMv2ncdEAbmCS+kXsXP3Myej9LOpK/69zYP1FURD3gcme/j/dUwKtdxL/XLWK/u/YmwFCG5L9bp1a/WTIxv0lunr+QjL6/oeaWPcM0Ej9NxhM98VhgwLiBgL48hfC/Tm26vh4d6T/EhbVAZLpPPi2Ez78CoCY9Fq+Bvdu8lr/HAZS+YZ8twCkyAb9RI+G/sTCBv9EYeEDa+5A+RyUSwGyRSkDvsua+NmIbv77U7L8='


def feature_register(food_id, features):
    if not food_id in repo:
        repo[food_id] = [features]
    else:
        repo[food_id] = np.concatenate((repo[food_id], [features]), axis=0)


def search(feat):
    # print("1个原始特征=", feat)
    # print("1个原始特征的特征长度=", len(feat))
    # print("1个原始特征的特征类型=", feat.shape, type(feat))
    s = pickle.dumps(feat)
    # print(s)
    print(len(s))  # 2199
    d = pickle.loads(s)
    # print(d)
    print(len(d))  # 512
    ids_results, scores_result = [], []
    if len(repo) <= 0:
        return ids_results, scores_result

    # 沿着现有的轴连接数组序列
    repo_concat = np.concatenate([repo[key] for key in repo], 0)
    all_repo_feats = np.array([x / np.linalg.norm(x) for x in repo_concat])
    print('特征个数=', len(repo))
    print('```````````````', len(all_repo_feats))

    all_repo_ids = []
    for id in repo:
        all_repo_ids += [id] * len(repo[id])

    print(all_repo_ids)
    print('-----------------------------------------------------------------')
    # feat = np.fromfile('feat.bin', dtype=np.float32)
    # 正则化（cos 之前）
    feat = feat / np.linalg.norm(feat)
    # 余弦相似度计算
    cos_distance = np.matmul(feat, all_repo_feats.transpose((1, 0)))
    ids_score_sorted = np.sort(cos_distance)[::-1]
    ids_indices_sorted = np.argsort(cos_distance)[::-1]
    all_repo_ids_sorted = np.array(all_repo_ids)[list(ids_indices_sorted)]

    food_ids, food_scores = [], []
    for j in range(len(all_repo_ids_sorted)):
        if all_repo_ids_sorted[j] not in food_ids:
            food_ids.append(all_repo_ids_sorted[j])
            food_scores.append(ids_score_sorted[j])

    for x in food_ids:
        ids_results.append(x)
    for x in food_scores:
        scores_result.append(round(float(100 * x), 2))

    return ids_results, scores_result


def getnp():
    feat = np.frombuffer(base64.b64decode(fea_base643), np.float32)
    return feat


if __name__ == '__main__':
    repo = {}
    fea = np.frombuffer(base64.b64decode(fea_base641), np.float32)
    feature_register('菜品1', fea)
    fea = np.frombuffer(base64.b64decode(fea_base642), np.float32)
    feature_register('菜品2', fea)
    fea = np.frombuffer(base64.b64decode(fea_base643), np.float32)
    feature_register('菜品3', fea)
    fea = np.frombuffer(base64.b64decode(fea_base641), np.float32)
    feature_register('菜品2', fea)

    # ids_results, scores_result = search(getnp())
    ids_results, scores_result = search(fea)
    print('=======================================================')
    print('-------ids_results:', ids_results)
    print('-----scores_result:', scores_result)

    print('---', len(fea_base641))